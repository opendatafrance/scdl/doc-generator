# validata-doc-generator

## Génération de documentation en Markdown

Générer un fichier Markdown pour un schéma :

```bash
python3 table_schema_to_md.py scdl-prenoms schema.json > output.md
```

Générer un fichier Markdown pour chacun des schémas de Validata :

```bash
python3 validata_schemas_to_md.py /path/to/validata-doc/schemas/
```
