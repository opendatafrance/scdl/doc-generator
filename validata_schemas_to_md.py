#!/usr/bin/env python3
"""
Downloads all table-schemas defined in validata_core and generates markdown files in a target directory.
"""
import argparse
import locale
import logging
import sys
from pathlib import Path

import toml
import validata_core

from table_schema_to_md import convert_source

log = logging.getLogger(__name__)


def main():
    locale.setlocale(locale.LC_ALL, '')  # Use environment variables.

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('target_dir', type=Path, help='path of target directory')
    parser.add_argument('--config', type=Path, help='use alternate `schemas.toml` config file')
    parser.add_argument('--log', default='WARNING', help='level of logging messages')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    target_dir = args.target_dir
    if not target_dir.exists():
        parser.error("Target dir {!r} not found".format(str(target_dir)))

    if args.config is not None:
        with args.config.open() as fp:
            schemas_config = toml.load(fp)
    else:
        schemas_config = validata_core.get_schemas_config()

    for schema_code, schema_config in schemas_config.items():
        log.info("Generating Markdown documentation for schema %s...", schema_code)
        schema = schema_config['schema']
        with (target_dir / '{}.md'.format(schema_code)).open('w') as mdout:
            convert_source(schema_code, schema, mdout, schemas_config)


if __name__ == '__main__':
    sys.exit(main())
