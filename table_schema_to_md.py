#!/usr/bin/env python3
#
#    Table schema to markdown
#
#    Pierre Dittgen, Jailbreak
"""
    Generates markdown page from JSON table schema file

    Table schema specs are defined by frictionlessdata.io:
    https://frictionlessdata.io/specs/table-schema/
"""

import argparse
import io
import locale
import logging
import sys
import urllib.parse
from collections import OrderedDict
from datetime import datetime
from pathlib import Path
from urllib.request import urljoin

import requests
import validata_core
from backports.datetime_fromisoformat import MonkeyPatch
from tableschema.config import REMOTE_SCHEMES

log = logging.getLogger(__name__)


MonkeyPatch.patch_fromisoformat()

SCHEMA_PROP_MAP = {
    'author': 'Auteur',
    'contributor': 'Contributeurs',
    'version': 'Version',
    'created': 'Schéma créé le',
    'updated': 'Schéma mis à jour le',
    'homepage': 'Site web',
    'example': 'Données d\'exemple',
}


TYPE_MAP = {
    'array': 'liste',
    'boolean': 'booléen',
    'date': 'date',
    'datetime': 'date et heure',
    'duration': 'durée',
    'geojson': '',
    'geopoint': 'point géographique',
    'integer': 'nombre entier',
    'number': 'nombre réel',
    'object': 'objet',
    'string': 'chaîne de caractères',
    'time': 'heure',
    'year': 'année',
    'year-month': 'année et mois',
}

FORMAT_MAP = {
    'email': 'adresse de courriel',
    'uri': 'adresse URL',
    'binary': 'données binaires encodées en base64',
    'uuid': 'identifiant UUID',

}

TYPE_SPECIFIC_MAP = OrderedDict([
    ('decimalChar', 'Séparateur décimal («.» par défaut)'),
    ('groupChar', 'Séparateur de groupes de chiffres («,» par défaut)'),
    # 'bareNumber' : 'Nombre nu', => Needs a specific treatment
    ('trueValues', 'Valeurs considérées comme vraies'),
    ('falseValues', 'Valeurs considérées comme fausses'),
])

CONSTRAINTS_MAP = OrderedDict([
    ('minLength', lambda v:  'Taille minimale : {}'.format(v)),
    ('maxLength', lambda v:  'Taille maximale : {}'.format(v)),
    ('minimum', lambda v: 'Valeur minimale : {}'.format(v)),
    ('maximum', lambda v: 'Valeur maximale : {}'.format(v)),
    ('pattern', lambda v: 'Motif : `{}`'.format(v)),
    ('enum', lambda v:  'Valeurs autorisées : {}'.format(", ".join(v))),
])


def format_format(format_val):
    """ Return markdown format information """
    return "- `{}` {}\n".format(format_val, FORMAT_MAP.get(format_val, ''))


def format_type_specific_info(col_content):
    """ Formats and return info relative to type """
    buff = io.StringIO()
    for prop in TYPE_SPECIFIC_MAP:
        if prop in col_content:
            buff.write('- {} : {}\n'.format(TYPE_SPECIFIC_MAP[prop], col_content[prop]))

    if 'bareNumber' in col_content and col_content['bareNumber'] == 'false':
        buff.write('- Le nombre peut contenir des caractères supplémentaires (« € », « % » ...)\n')
    ret = buff.getvalue()
    buff.close()
    return ret


def format_constraints(col_content):
    """ Converts type and constraints information into markdown """
    buffer = io.StringIO()

    # Type
    type_ = col_content.get('type')
    format_ = col_content.get('format')

    if type_:
        type_val = TYPE_MAP.get(type_, '??{}??'.format(type_))
        buffer.write('- Type : {}{}\n'.format(
            type_val,
            "" if format_ == "default" else " (format `{}`)".format(format_)

        ))
        # Type specific properties
        buffer.write(format_type_specific_info(col_content))

    # RDFType
    rdf_type = col_content.get('rdfType')
    if rdf_type:
        buffer.write('- Type RDF : {}\n'.format(rdf_type))

    example = col_content.get('example')
    if example:
        buffer.write('- Exemple : {}\n'.format(example))

    constraints = col_content.get('constraints') or {}
    required = constraints.get('required')
    if required is None:
        required = False
        constraints["required"] = required

    constraint_str_list = list(filter(None, [
        'obligatoire' if required else 'optionnelle',
        'unique' if constraints.get('unique') else None,
    ]))
    buffer.write('- Valeur : {}\n'.format(",".join(constraint_str_list)))

    # minLength, maxLength, minimum, maximum, pattern, enum
    for prop in CONSTRAINTS_MAP:
        if prop in constraints:
            buffer.write('- {}\n'.format(CONSTRAINTS_MAP[prop](constraints[prop])))

    ret = buffer.getvalue()
    buffer.close()
    return ret


def format_property(name, value):
    if name in {"created", "updated"}:
        return datetime.fromisoformat(value).strftime("%x")
    return value


def convert_source(schema_code, source, out_fd, schemas_config=None):
    log.info('Loading schema from %r', source)
    schema = validata_core.Validator(schemas_config).load_schema(source)

    # Try to load CONTEXT.md
    context_md_filename = "CONTEXT.md"
    log.info('Loading %r from %r', context_md_filename, source)
    context_md_text = load_text_file(context_md_filename, source)

    # Try to load SEE_ALSO.md
    see_also_md_filename = "SEE_ALSO.md"
    log.info('Loading %r from %r', see_also_md_filename, source)
    see_also_md_text = load_text_file(see_also_md_filename, source)

    convert_json(schema_code, schema.descriptor, context_md_text, see_also_md_text, out_fd)


def convert_json(schema_code, schema_json, context_md_text, see_also_md_text, out_fd):
    """ Converts table schema data to markdown """

    # Header
    out_fd.write('# {}'.format(schema_json['title']))
    out_fd.write('\n\n')

    if 'description' in schema_json:
        out_fd.write(schema_json['description'])
        out_fd.write('\n\n')

    version = schema_json.get('version')
    if version:
        out_fd.write("## Version {}".format(version, ))
        out_fd.write('\n\n')

    for property_name in ('author', 'contributor', 'created', 'updated', 'homepage', 'example'):
        property_value = schema_json.get(property_name)
        if property_value:
            out_fd.write('- {} : {}\n'.format(SCHEMA_PROP_MAP[property_name],
                                              format_property(property_name, property_value)))

    out_fd.write(
        '- [Gabarit au format Excel (xlsx)](https://scdl.opendatafrance.net/docs/templates/{}.xlsx)\n'.format(schema_code))

    # Missing values
    missing_values = schema_json.get('missingValues')
    if missing_values:
        out_fd.write('- Valeurs manquantes : {}\n'.format(", ".join(map(lambda v: '`"{}"`'.format(v), missing_values))))

    # Primary key
    primary_key = schema_json.get('primaryKey')
    if primary_key:
        out_fd.write('- Clé primaire : `{}`\n'.format(
            ", ".join(primary_key) if isinstance(primary_key, list) else primary_key))

    # Foreign keys contraint is more complex than a list of strings, more work required.

    out_fd.write("\n\n")

    if context_md_text is not None:
        out_fd.write("## Contexte\n\n")
        out_fd.write(context_md_text)
        out_fd.write("\n\n")

    fields = schema_json.get('fields')
    if fields:
        out_fd.write('## Modèle de données\n\n')
        out_fd.write(
            "Ce modèle de données fait partie et respecte les exigences du "
            "[Socle Commun des Données Locales](https://scdl.opendatafrance.net/docs/). "
            "Il repose sur les {} champs suivants correspondant aux colonnes du fichier tabulaire.\n\n".format(
                len(fields)))
        for field in fields:
            convert_field(field, out_fd)
        out_fd.write("\n\n")

    if see_also_md_text is not None:
        out_fd.write("## Voir aussi\n\n")
        out_fd.write(see_also_md_text)
        out_fd.write("\n\n")


def convert_field(field_json, out_fd):
    """ Convert json content describing a column to markdown """

    field_name = field_json.get('name')
    out_fd.write('### {}\n\n'.format(
        "`{}`".format(field_name) if field_name else "Erreur : nom manquant"
    ))

    title = field_json.get('title')
    if title:
        out_fd.write("- Titre : {}\n".format(title))

    description = field_json.get('description')
    if description:
        out_fd.write("- Description : {}\n".format(description))

    out_fd.write(format_constraints(field_json))
    out_fd.write('\n')


def load_text_file(filename, source):
    # Remote
    if urllib.parse.urlparse(source).scheme in REMOTE_SCHEMES:
        text_file_source = urljoin(source, filename)
        response = requests.get(text_file_source)
        if not response.ok:
            log.info("Could not fetch %r: %r", text_file_source, response)
            return None
        return response.text

    # Local
    if isinstance(source, str):
        text_file_source = Path(source).parent / filename
        return text_file_source.read_text()

    return None


def main():
    """Converts a table schema file into Markdown."""
    locale.setlocale(locale.LC_ALL, '')  # Use environment variables.

    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('schema_code', help='code of the schema (example: scdl-prenoms)')
    parser.add_argument('table_schema', help='path or URL of table schema file')
    parser.add_argument('-o', '--output', help='Output file name', default='stdout')
    parser.add_argument('--log', default='WARNING', help='level of logging messages')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    out_fd = sys.stdout if args.output == 'stdout' else open(args.output, mode='wt', encoding='UTF-8')

    convert_source(args.schema_code, args.table_schema, out_fd)


if __name__ == '__main__':
    main()
